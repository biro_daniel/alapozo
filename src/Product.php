<?php

class Product
{
    /*private float $price;
    private string $name;*/
    // private string $description;

    //konstruktror akkor fut, amikor a new parancs
    /**
     * @param string $name
     * @param float $price
     */
    public function __construct(
        private float $price, // ajánlott megadni a típust
        private string $name) // lehetne itt default értékeket megadni
    {
        // echo 'fut a construct'; //(__LINE__, __FUNCTION)
        // $this->name = $name . ' - teszt';
        //$this->price = $price; // -> ez akkor kell ha a konstruktoron kívül határozzuk meg a típusokat és a hatóköröket
    }

    // lehessen a termék árához áfát adni
    /**
     * @param float $rate
     * @param Product
     */
    public function addTax(float $rate): Product
    {
        $this->price += $this->price * $rate / 100;
        // láncolhatóság
        return $this;
    }

    // lehessen árleszállítást csinálni
    public function addDiscount(float $rate): Product // lehet privát is az eljárás
    {
        $this->price -= $this->price * $rate / 100;
        // láncolhatóság
        return $this;
    }

    // ár visszaadása mert private
    public function getPrice(){
        return $this->price;
    }

    // név visszaadása mert private
    public function getName(){
        return $this->name;
    }

    // termék kiírása
    public function display()
    {
        $output = '<article class="product">';
        $output .= "<h2>{$this->name}</h2>";
        $output .= "<div><b>Ára:</b> {$this->price}.-HUF</div>";
        $output .= '</article>';
        return $output;
    }

    // objektum echo esetén a displayt adja vissza
    public function __toString(): string
    {
        return $this->display();
    }

    public function __destruct()
    {
        echo 'Fut a destructor';
    }
}
