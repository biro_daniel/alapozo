<?php

namespace App\Controllers;

use App\Invoice;
use App\View;

class InvoiceController
{
    public function index(): string
    {
        return View::make('invoices/index')->render();
    }

    public function create(): string
    {
        return View::make('invoices/create')->render();
    }

    public function store()
    {
        //$invoice = new Invoice();
        // adatok hibakezelése, művelet a hibakezelt adatokkal: modell
        $amount = filter_input(INPUT_POST, 'amount');

        //$invoice->create($amount);

        var_dump($amount);
    }
}