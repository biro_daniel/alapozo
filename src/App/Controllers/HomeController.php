<?php
declare(strict_types=1);

namespace App\Controllers;

use App\Models\Invoice;
use App\Models\SignUp;
use App\Models\User;
use App\View;
use App\InvalidFileTypeException;
use App\InvalidFileSizeException;
use App\Utility as Utility;
use Faker\Factory;

class HomeController
{
    public function index(): View
    {
        $faker = Factory::create('hu_HU');

        $name = $faker->name();
        $email = $faker->email();

        $amount = 4990; //teszt érték

        $userModel = new User();
        $invoiceModel = new Invoice();

        //SignUp folyamat minta
        $invoiceId = (new SignUp($userModel, $invoiceModel))->register(
            [
                'email' => $email,
                'name' => $name
            ],
            [
                'amount' => $amount
            ]
        );

        // kérjük le a bevitt adatokat (teszt...)

        return View::make('index', ['title' => 'Home Page', 'invoice' => $invoiceModel->find($invoiceId)]);
    }

    public function fileSelection(): View
    {
        return View::make('fileSelection');
    }

    public function download()
    {
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="testfile.pdf"');

        readfile(STORAGE_PATH . 'bills/arvizturo-tukorfurogep.pdf');
    }

    public function upload(): string|InvalidFileSizeException|InvalidFileTypeException
    {
        $fileCount = 0;
        $finalFiles = [];
        $rawFileNames = [];
        $fileTypes = $_FILES['filesToUpload']['type'];
        $fileSizes = $_FILES['filesToUpload']['size'];
        $fileNames = $_FILES['filesToUpload']['name'];

        try {
            $this->typeCheck($fileTypes);
        } catch (InvalidFileTypeException $e) {
            echo $e->getMessage() . PHP_EOL;
            die();
        }

        try {
            $this->sizeCheck($fileSizes);
        } catch (InvalidFileSizeException $e) {
            echo $e->getMessage() . PHP_EOL;
            die();
        }

        foreach ($fileNames as $key => $value) {
            $value = $this->slugify($value);
            //array_push($rawFileNames, $value);
            $value = $this->existenceCheck($value, $rawFileNames);
            $filePath = STORAGE_PATH . '/bills/' . $value;
            move_uploaded_file(
                $_FILES['filesToUpload']['tmp_name'][$key],
                $filePath
            );
            if ($value <> '') {
                $fileCount++;
                array_push($finalFiles, $value);
            }
        }

        $finalFiles = implode('<br>', $finalFiles);

        header('Location: /'); //átirányítás: vissza a főoldalra (+param: , response_code: 313)
        exit;
        //unlink(STORAGE_PATH . '/bills/arvizturo-tukorfurogep.pdf');

        //return "$finalFiles <br> Összesen $fileCount fájl került feltöltésre.";
    }

    /**
     * @throws InvalidFileTypeException
     */
    public function typeCheck(array $fileTypes): bool|InvalidFileTypeException
    {
        if (count(array_filter($fileTypes, function ($fileType) {
                return ($fileType === 'application/pdf' || $fileType === '');
            })) <> count($fileTypes)) {
            throw new InvalidFileTypeException();
        } else {
            return true;
        }
    }

    /**
     * @throws InvalidFileSizeException
     */
    public function sizeCheck(array $fileSizes): bool|InvalidFileSizeException
    {
        $validity = true;
        foreach ($fileSizes as $fileSize) {
            if ($fileSize > (1024 * 1024)) {
                $validity = false;
            }
        }
        if ($validity === false) {
            throw new InvalidFileSizeException();
        }
        return true;
    }

    public function phpinfo()
    {
        \phpinfo();
    }

    public function slugify($text, string $divider = '-'): string
    {
        // source: https://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string
        // replace non letter or digits by divider
        $text = $this->textBeautifier($text);
        $text = preg_replace('/[^a-z.0-9]/i', $divider, $text);

        // transliterate
        $text = iconv('UTF-8', 'ASCII//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-.\w]+~', '', $text);

        // trim
        $text = trim($text, $divider);

        // remove duplicate divider and replace '-.'
        $text = preg_replace('~-+~', $divider, $text);
        $text = preg_replace('~-\.+~', '.', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return '';
        }

        return $text;
    }

    function textBeautifier($strings): array|string
    {
        $orig_characters = ['á', 'é', 'í', 'ó', 'ö', 'ő', 'ú', 'ü', 'ű', 'Á', 'É', 'Í', 'Ó', 'Ö', 'Ő', 'Ú', 'Ü', 'Ű'];
        $transformed_characters = ['a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u'];
        return str_replace($orig_characters, $transformed_characters, $strings);
    }

    public function existenceCheck(string $fileName, array $rawFileNames): string //countCheck?
    {
        $count = array_filter($rawFileNames, function ($file) {
            //return ($file === $fileName);
            return true;
        });
        $dir = new \DirectoryIterator(STORAGE_PATH . '/bills/');
        foreach ($dir as $file) {
            if ($file->getFilename() === $fileName) {
                // keresés bővítése: mappában lévő fájlok végéről le kell vágni a számokat és úgy kell megszámolni, hogy hány van az adott nevű fájlból
                // mielőtt hozzáfűzném a sorszámot, előtte kell benyomni egy tömbb, ahonnan meg lehet majd számolni az azonos nevű fájlokat
                $fileName = substr($fileName, 0, strlen($fileName) - 4) . '-2' . substr($fileName, -4, 4);
            }
        }
        return $fileName;
    }
}
