<?php

namespace App;

class CollectionAgency implements DebtCollector // egy osztály használhat több interfészt is
{

    public function collect(float $owedAmount): float
    {
        $minAmount = $owedAmount * .5;

        return mt_rand($minAmount, $owedAmount); // business logic
    }

}