<?php

namespace App\PaymentGateway\Otp;


class Transaction
{
	private float $amount; // érdekel minket, hogy ez itt belül mi mert kívül használjuk
	
    public function __construct($amount)
    {
        $this->amount = $amount;
    }
/*	
	// getter - kiolvasás
	public function getAmount(): float
	{
		return $this->amount;
	}
	
	// setter - beállítás
	public function setAmount(): void
	{
		$this->amount = $amount;
	}*/
	
	public function process()
	{
		echo "<br> {$this->amount}.- HUF összegű tranzakció feldolgozása";
		
		// számla készítése
		$this->generateBill();
		
		// e-mail küldése
		$this->sendEmail();
		
	}
	
	private function generateBill() // nincs igazán értelme publikussá tenni őket, nem kell külsőleg futtatni
	{
		return true;
	}
	
	private function sendEmail()
	{
		return true;
	}
}
