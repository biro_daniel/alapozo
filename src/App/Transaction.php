<?php

namespace App;

/**
 * @property-read string $a
 * @property-write float $b
 * @property int $c
 */
class Transaction
{
    public array $data = []; #itt lesznek a már definiált propertyk
    /**
     * @var array|string[]
     */
    protected array $validPropertyTypes = [
        'a' => 'string',
        'b' => 'double'|'integer',
        'c' => 'integer'
    ];
    /**
     * @var array|array[]
     */
    protected array $validProperties = [
        'a' => [
            'types' => ['string'],
            'mode' => 'readonly'
        ],
        'b' => [
            'types' => ['double','string'],
            'mode' => 'writeonly'
        ],
        'a' => [
            'types' => ['integer'],
            'mode' => null
        ]
    ];

    /**
     * @todo HF:
     * az órán bemutatott és megadott getterek és setterek újbóli megvalósítása de most a $validPropertyTypes
     * vagy (és ez a jobb) $validProperties tömbök felhasználásával
     */

    // 1. megoldás: változók értékei beégetve
/*    public function __get(string $name)
    {
        if (!in_array($name, ['a', 'c'])) {
            echo "You are trying to reach an invalid property [$name]" . PHP_EOL;
            return '';
        }
        if ($name === 'a' && !array_key_exists($name, $this->data)) {
            $this->data['a'] = $this->generateRandomString();
        }
        if ($name === 'c' && !array_key_exists($name, $this->data)) {
            $this->data[$name] = null;
        }

        return $this->data[$name];
    }

    public function __set(string $name, $value): void
    {
        if (!in_array($name, ['b', 'c'])) {
            echo "You are trying to set an invalid property [$name]" .PHP_EOL;
        } elseif ($name === 'b' && !is_double($value)) {
            trigger_error('Property b must be a double!');
        } elseif ($name === 'c' && !is_int($value)) {
            trigger_error('Property c must be an integer!');
        }
        else {
            $this->data[$name] = $value;
        }
    }*/

    // 2. megoldás: $validPropertyTypes felhasználásával
    public function __get(string $name)
    {
        if (!array_key_exists($name, $this->validPropertyTypes)) {
            echo "You are trying to reach an invalid property [$name]" . PHP_EOL;
            return '';
        }
        // ez a rész itt egyelőre nem implementálható a bemeneti tömb alapján
        /*if ($name === 'a' && !array_key_exists($name, $this->data)) {
            $this->data['a'] = $this->generateRandomString();
        }
        if ($name === 'c' && !array_key_exists($name, $this->data)) {
            $this->data[$name] = null;
        }*/

        $this->data[$name] = $this->generateRandomString();;
        return $this->data[$name];
    }

    public function __set(string $name, $value): void
    {
        if (!array_key_exists($name, $this->validPropertyTypes)) {
            echo "You are trying to set an invalid property [$name]" . PHP_EOL;
        } elseif (!str_contains($this->validPropertyTypes[$name], gettype($value))) {
            trigger_error('Property ' . $name . ' must be a(n) ' . $this->validPropertyTypes[$name] . '!');
        }
        else {
            $this->data[$name] = $value;
        }
    }

    /*
    // 3. megoldás: $validProperties felhasználásával
    public function __get(string $name)
    {

    }

    public function __set(string $name, $value): void
    {

    }*/

    protected function generateRandomString(int $length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
