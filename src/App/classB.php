<?php

namespace App;

class classB extends classA
{
    protected static string $name = 'B';

    public function getName(): string
    {
        return self::$name;
    }
}
