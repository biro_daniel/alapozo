<?php

namespace App\Field;

interface Renderable
{
    public function render(): string; // ez a mit (csak public vagy protected lehet ha absztrakt de itt már nem az)
}