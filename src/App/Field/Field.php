<?php

namespace App\Field;

abstract class Field implements Renderable
{
    protected string $newclass = 'form-input';
    public function __construct(protected string $name, protected string | array $class='')
    {
//        if (!is_array($class) && !is_string($class)) {
//            trigger_error("Class parameter must be string or array!");
//        }

        if (is_array($class)){
            $this->newclass = $this->newclass.' '.implode(' ', $class);
        } else {
            $this->newclass = $this->newclass.' '.$this->class;
        }
        $this->class = $this->newclass;
    }
}
/**@todo Órai feladat:
 * kiegészítés:
 * +textarea
 * +email (ez a text bővítése)
 * +kiegészítés: legyen a mezőknek egy olyan osztálya, hogy 'form-input' de egy nem kötelező tulajdonságként lehessen hozzáfűzni string vagy stringekből álló t9mbből 1 vagy több osztályt
 * pl. <input type="text" name="name" class="form-input elemei">
 * ['one,', 'two', 'three'] -> class="form-input one two three"
*/
