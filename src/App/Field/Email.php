<?php

namespace App\Field;

class Email extends Text
{
    public function render(): string
    {
        return '<input type="email" name="'.$this->name.'" class="'.$this->class.'">';
    }
}