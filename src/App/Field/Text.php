<?php

namespace App\Field;

class Text extends Field
{

    public function render(): string // default értékkel lehet paramétert is megadni
    { // megvalósítás: hogyan
        return '<input type="text" name="'.$this->name.'" class="'.$this->class.'">';
    }
}