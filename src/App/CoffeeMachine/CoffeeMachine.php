<?php
/** @todo HF: legyen az egész Coffee**** osztályhalmaz egy mappa része és ennek megfelelően módosuljanak a névterek */
namespace App\CoffeeMachine;

class CoffeeMachine
{
    public function makeEspresso(){
        echo static::class . '| Espresso készítése...'. PHP_EOL;
    }

    /* feladatokhoz:
    public function orderTest(){
        echo 'Ez a CoffeeMachine baseclass. Én vagyok az első.';
    }

    public function orderTest2(){
        echo 'A baseclass a második.';
    }
    */

}