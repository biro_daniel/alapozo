<?php

namespace App\CoffeeMachine;

class LatteMachine extends CoffeeMachine
{
    use LatteTrait;

    //protected string $typeOfMilk = "Sovány tej"; //csak ugyanazt adhatom meg, mint a traitben, nem írható felül


    /* feladatokhoz:
    public function orderTest(){
        echo 'Ez a LatteMachine childclass. Én vagyok az első.';
    }

    public function getMilk(){
        echo 'get milk is working';
    }

    public function makeEspresso()
    {
        echo static::class . '| Presszó kávé készítése (A makeEspresso felülírása a Latte child classból).'. PHP_EOL;
    }
    */
}