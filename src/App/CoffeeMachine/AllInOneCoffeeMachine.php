<?php

namespace App\CoffeeMachine;

class AllInOneCoffeeMachine extends CoffeeMachine // implements LatteMachine, CappuccinoMachine // ha interface lenne ez a kettő, akkor lehetne behúzni
{
    use CappuccinoTrait{
        CappuccinoTrait::makeCappuccino as public;
    }
    use LatteTrait;
}