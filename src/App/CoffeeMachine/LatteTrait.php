<?php

namespace App\CoffeeMachine;

trait LatteTrait
{
    /* feladatokhoz:
    public static array $test = [];

    abstract public function getMilk();

    public function orderTest(){
        echo 'Ez a LatteTrait trait. Én vagyok az első.';
    }

    public function orderTest2(){
        echo 'A trait a második.';
    }

    public function makeEspresso()
    {
        echo static::class . '| Presszó kávé készítése (A makeEspresso felülírása a Latte traitből).'. PHP_EOL;
    }
    */

    private string $typeOfMilk = "Teljes tej";
    public static string $a = 'property teszt...'. PHP_EOL;

    public function makeLatte(){
        echo static::class . '| Latte készítése ['. $this->typeOfMilk .']'. PHP_EOL;
    }

    public static function test()
    {
        echo 'Ez egy teszt...'.PHP_EOL;
    }

    public function setTypeOfMilk(string $typeOfMilk): static
    {
        $this->typeOfMilk = $typeOfMilk;

        return $this;
    }
}