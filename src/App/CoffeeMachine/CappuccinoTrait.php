<?php

namespace App\CoffeeMachine;

trait CappuccinoTrait
{
    private function makeCappuccino()
    {
        echo static::class . '| Cappuccino készítése...'. PHP_EOL;
    }
}