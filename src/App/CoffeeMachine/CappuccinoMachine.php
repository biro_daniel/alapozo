<?php

namespace App\CoffeeMachine;

class CappuccinoMachine extends CoffeeMachine
{
    use CappuccinoTrait {
        CappuccinoTrait::makeCappuccino as public;
    }
}