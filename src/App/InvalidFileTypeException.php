<?php

namespace App;

class InvalidFileTypeException extends \Exception
{
    protected $message = 'One or more invalid file types. Please try again with only pdf files.';
}
