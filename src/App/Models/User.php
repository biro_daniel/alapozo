<?php

namespace App\Models;

use App\App;
use App\Model;

class User extends Model
{
    public function create(string $email, string $name, bool $isActive = false): int
    {
        $stmt = $this->db->prepare(
            'INSERT INTO users (email, name, is_active, time_created)
                        VALUES(?, ?, ?, NOW())'
        );

        $stmt->execute([$email, $name, $isActive]);

        return (int) $this->db->lastInsertId();
    }

    //createMany(...count)->külön eljárás több felhasználó egyszerre történő létrehozására
}