<?php

namespace App\Models;

use App\App;
use App\Model;

class SignUp extends Model
{

    /**
     * @param User $userModel
     * @param Invoice $invoiceModel
     */
    public function __construct(protected User $userModel, protected Invoice $invoiceModel)
    {
        $this->db = App::db();
        parent::__construct($this->db);
    }

    public function register(array $userInfo, array $invoiceInfo): int
    {
        try {
            $this->db->beginTransaction(); //tranzakció indítása
            //User Model terv:

            $userId = $this->userModel->create($userInfo['email'], $userInfo['name'], true); // a beillesztett userId-val vagy false értékkel tér vissza
            // az adatátadás szebben megvalósítható dto megoldással

            $invoiceId = $this->invoiceModel->create($invoiceInfo['amount'], $userId); // a beillesztett invoiceId-val vagy false értékkel tér vissza

            $this->db->commit();
        } catch (\Throwable $e) {
            if ($this->db->inTransaction()) {
                $this->db->rollBack();
            }
            throw $e;
        }

        return $invoiceId;
    }
}