<?php

namespace App;

class ClassC
{

    public function __construct(public int $a, public int $b)
    {
    }

    public function test()
    {
        return 'test';
    }

    public function bar(): object
    {
        return new class($this->a, $this->b) extends ClassC { // ~callback
            public function __construct(public int $a, public int $b)
            {
                parent::__construct($a, $b);
                echo $this->a;
            }
        };
    }
}