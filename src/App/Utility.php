<?php
declare(strict_types=1);

namespace App;

class Utility
{
    /**
     * @throws InvalidFileTypeException
     */
    public function typeCheck(array $fileTypes): bool | InvalidFileTypeException
    {
        if (count(array_filter($fileTypes, function ($fileType) {
                return ($fileType === 'application/pdf' || $fileType === '');
            })) <> count($fileTypes)) {
            throw new InvalidFileTypeException();
        } else {
            return true;
        }
    }

    /**
     * @throws InvalidFileSizeException
     */
    public function sizeCheck(array $fileSizes): bool | InvalidFileSizeException
    {
        $validity = true;
        foreach ($fileSizes as $fileSize) {
            if ($fileSize > (1024 * 1024)) {
                $validity = false;
            }
        }
        if ($validity === false) {
            throw new InvalidFileSizeException();
        }
        return true;
    }

    public function phpinfo()
    {
        \phpinfo();
    }

    public function slugify($text, string $divider = '-'): string
    {
        // source: https://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string
        // replace non letter or digits by divider
        $text = $this->textBeautifier($text);
        $text = preg_replace('/[^a-z.0-9]/i', $divider, $text);

        // transliterate
        $text = iconv('UTF-8', 'ASCII//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-.\w]+~', '', $text);

        // trim
        $text = trim($text, $divider);

        // remove duplicate divider and replace '-.'
        $text = preg_replace('~-+~', $divider, $text);
        $text = preg_replace('~-\.+~', '.', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return '';
        }

        return $text;
    }

    function textBeautifier($strings): array|string {
        $orig_characters = ['á', 'é', 'í', 'ó', 'ö', 'ő', 'ú', 'ü', 'ű', 'Á', 'É', 'Í', 'Ó', 'Ö', 'Ő', 'Ú', 'Ü', 'Ű'];
        $transformed_characters = ['a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u'];
        return str_replace($orig_characters, $transformed_characters, $strings);
    }

    public function existenceCheck(string $fileName): string
    {
        $dir = new \DirectoryIterator(STORAGE_PATH . '/bills/');
        foreach ($dir as $file) {
            if ($file->getFilename() === $fileName) {
                // keresés bővítése: mappában lévő fájlok végéről le kell vágni a számokat és úgy kell megszámolni, hogy hány van az adott nevű fájlból
                // mielőtt hozzáfűzném a sorszámot, előtte kell benyomni egy tömbb, ahonnan meg lehet majd számolni az azonos nevű fájlokat
                $fileName = substr($fileName, 0, strlen($fileName) - 4) . '-2' . substr($fileName, -4,  4);
            }
        }
        return $fileName;
    }

}