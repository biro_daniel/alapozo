<?php

namespace App;

class DebtCollectionService
{
    public function collectDebt(DebtCollector $collector) // most beégettük ide a $collector változót, túl konkrét
    {
        //var_dump($collector instanceof DebtCollector); // a Guynak és a DebtCollectornak is példánya!
        $owedAmount = mt_rand(200, 1000);
        $collectedAmount = $collector->collect($owedAmount);

        echo 'A begyűjtött összeg alapja: '.$owedAmount.' | A begyűjtött összeg: '.$collectedAmount.'<br>';
    }
}