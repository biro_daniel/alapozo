<?php

namespace App;

class InvalidFileSizeException extends \Exception
{
    protected $message = 'One or more files exceed the authorized file size. Please try again with files of maximum 1 MB.';
}