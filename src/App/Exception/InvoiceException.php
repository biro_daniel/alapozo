<?php

namespace App\Exception;

class InvoiceException extends \Exception
{
    public static function missingBillingInfo():static
    {
        return new static('Hiányzó számlázási adatok');
    }

    public static function invalidAmount():static
    {
        return new static('Érvénytelen összeg');
    }
}