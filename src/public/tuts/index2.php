<?php
require __DIR__ . "/../../vendor/autoload.php";

use App\PaymentGateway\Otp\Transaction;
// Encapsulation - zártság elve
// Abstraction - absztrakció
// Inheritance - öröklődés
// Polymorphism - többalakúság

$transaction = new Transaction(1500);
//$transaction = new Transaction(990);
/*
$reflectionProperty = new ReflectionProperty(Transaction::class, 'amount'); // beépített osztály

$reflectionProperty->setAccessible(true);

$reflectionProperty->setValue($transaction, 990);
var_dump('<pre>', $reflectionProperty->getValue($transaction));
*/

//$transaction->setAmount(990);
//var_dump($transaction->getAmount());
$transaction->process();