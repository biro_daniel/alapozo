<?php
/** @todo Órai feladat: 5 db függvény (2 paraméterrel) + demo hívások (2-3 db / funkció) */

declare(strict_types=1); //a PHP ugye alapvetően nem erősen típusos

//Összeadás
function sum($a, $b)
{
    if (is_numeric($a) && is_numeric($b)) {
        return $a + $b;
    } else {
        return 'Mindkét bemeneti paraméter szám legyen!';
    }
}

//Kivonás
function subtract($a, $b)
{
    if (is_numeric($a) && is_numeric($b)) {
        return $a - $b;
    } else {
        return 'Mindkét bemeneti paraméter szám legyen!';
    }
}

//Szorzás
function multiply($a, $b)
{
    if (is_numeric($a) && is_numeric($b)) {
        return $a * $b;
    } else {
        return 'Mindkét bemeneti paraméter szám legyen!';
    }
}

//Osztás
function divide($a, $b)
{
    if (is_numeric($a) && is_numeric($b)) {
        return fdiv($a, $b);
    } else {
        return 'Mindkét bemeneti paraméter szám legyen!';
    }
}

//Hatványozás
function exponent($a, $b)
{
    if (is_numeric($a) && is_numeric($b)) {
        return pow($a, $b);
    } else {
        return 'Mindkét bemeneti paraméter szám legyen!';
    }
}

//hívások
echo sum(2, 4);
echo '<br>';
echo sum('asdf', null);
echo '<br>';

echo subtract(2, 4);
echo '<br>';

echo multiply(2.5, 4.32);
echo '<br>';

echo divide(21500, 0);
echo '<br>';

echo exponent(2, 8);
echo '<br>';

/*------------------*/
function name(): ?string
{ // visszatérési érték típusa: lehet void is, kérdőjellel: nullsafe
    //echo 'Hello';
    //return 1;
    return 'Biró Dániel';
}

function name2($x): string|int|float|array|bool
{ // void mixed     ?int -> nullable <= union tyoes |||
    return $x;
}

function name3(): mixed
{
    return rand(1, 100) % 2 === 0 ? 50 : 'nonono';
}

echo name3();
echo '<br>';
var_dump(name3()); //visszatérési érték nélkül NULL jön "eredményül"

//szorzás
function multiply2(int $a, int|float $b = 10): int|float
{ // default érték megadása, az ilyen paramétereket mindig a végén kell megadni
    return $a * $b;
}

echo '<br>';
$result0 = multiply2(3); // default érték használata
$result = multiply2(3, 6.00);
var_dump($result);

function multiplyDouble(int|float &$a, int|float $b): int|float
{
    //referencia átadás
    $a *= 2;
    return $a * $b;
}

$a = 3;
$b = 4;
echo '<pre>';
echo multiplyDouble($a, $b);
var_dump($a, $b);

// ... operátor (unpack / spread operator)
function multiplyAnyAmountOfNumbers(...$numbers) /*: int | float*/
{ //ehelyett: int|float $a, int|float $b, array $numbers
    $ret = 1;
    foreach ($numbers as $number) {
        $ret *= $number;
    }
    return $ret;
}

$numbers = [5, 6, 7, 8];
$a = 3;
$b = 4;
echo multiplyAnyAmountOfNumbers($a, $b, 5, 6, 7, 8);
echo '<br>';
echo multiplyAnyAmountOfNumbers($a, $b, ...$numbers); // ugyanazt kapjuk

echo '<br>';
var_dump($numbers);
echo '<br>';
var_dump(...$numbers);

//osztás
function divide2(int|float &$a, int|float $b): int|float|bool
{
    if ($b === 0) {
        return false;
    } elseif ($a % $b === 0) {
        return fdiv($a, $b);
    }
    return $a;
}

$a = 8;
$b = 4;

echo divide2($a, $b);
echo '<br>';
echo divide2(b: $b,a: $a); // named arguments: a PHP8-ban jött be
echo '<br>';

$array = [ //asszociatív tömb
    'a' => 8,
    'b' => 4
];
echo divide2(...$array);
echo '<br>';

// setCookie('test1', 'value1', 0, '', '', false, true);
// setCookie('test2', 'value2', httponly: true); // analóg az előzővel

// scope
test($a);
function test(&$a) {
    //global $a;
    echo ++$GLOBALS['a']; // nem javasolt a global tömb használata
}
echo '<br>';
var_dump($a);

// változónév eljárások
$a = 9;
$b = 3;
$fv = 'divide2';

if (is_callable($fv)) {
    echo $fv($a, $b);
} else {
    echo 'Nincs ilyen eljárás: ' . $fv;
}
echo '<br>';
