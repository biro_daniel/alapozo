<?php
/**
 * Név hatókör: @link:https://www.php.net/manual/en/language.namespaces.rules.php
 * PSR sztenderdek: @link:https://www.php-fig.org/psr/  (1-es és 12-es szabvány, esetleg a 4-es)
 */

/*
require_once "../../App/PaymentGateway/PayPal/Transaction.php";
require_once "../../App/PaymentGateway/Otp/Transaction.php";
require_once "../../App/PaymentGateway/Otp/CustomerProfile.php";
require_once "../../App/Notification/Email.php";
*/
// require_once "../../App/PaymentGateway/Otp/DateTime.php";

spl_autoload_register( function ($classname) {
    $classPath = __DIR__.'/../../'.str_replace('\\', '/', $classname).'.php';
    if (is_file($classPath)) { // Autoloader implementations must not throw exceptions!
        require $classPath;
    }
});

/*spl_autoload_register( function ($classname) {
    //var_dump($classname);
    echo 'autoload 2';
}, prepend: true);*/

//use App\PaymentGateway\Otp\Transaction; // ez a "megszokott" import
use App\PaymentGateway\PayPal\Transaction as PayPalTransaction;
use App\PaymentGateway\Otp;
//use App\PaymentGateway\Otp\{Transaction as otpTransaction, CustomerProfile}; // csoportos import

$paypalTransaction = new PayPalTransaction(); // minősített osztálynév: qualified classname: \..\..
var_dump($paypalTransaction);

$otpTransaction = new Otp\Transaction();
var_dump($otpTransaction);

$otpCustomerProfile = new Otp\CustomerProfile();
var_dump($otpCustomerProfile);

/*
$transaction = new Transaction();
var_dump($transaction); // a use miatt a PayPalből származik
*/
