<?php

require __DIR__."/../../vendor/autoload.php";

use App\Address\Address as Address;
echo '<pre>';

/*
$data = [
    'address_line_1' => 'test 1',
    'address_line_2' => 'test 2',
    'city_name' => 'városnév',
    'country_name' => 'Országnév',
    'postal_code' => '1234',
    //'valami' => 'helo',
    'time_created' => 5678
];
$address = new Address($data);

$address->address_type_id = 3;
echo $address->address_type_id;


echo '<br>'.$address->time_created;
var_dump($address);
//legyen egy display ami visszaad egy html 'card' elemet (bootstrap classokkal)
echo $address->display();
//toString használatával is menjen : echo $address
echo $address;
//fakerrel feltöltött objektum létrehozása

//faker objektum készítése | a cimtipus legyen random


//Residental cím tipus teszt
//new

//display

//Shipping cím tipus teszt
//new

//display

//Temporary cím tipus teszt
//new

//display
*/

// saját kód:
$address_count = 50;
for ($i = 0; $i < $address_count; $i++) {
    $fakerObj = Faker\Factory::create('hu_HU');

    $data = [
        'address_line_1' => $fakerObj->streetAddress(),
        'address_line_2' => $fakerObj->secondaryAddress(),
        'city_name' => $fakerObj->city(),
        'country_name' => $fakerObj->country(),
        'postal_code' => $fakerObj->postcode(),
        //'valami' => 'helo',
        'time_created' => time()
    ];

    $address = new Address($data);
    $address->address_type_id = rand(1, 3);
    echo $address->display();
    echo '<br>';
}
