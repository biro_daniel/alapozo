<?php
declare(strict_types = 1);
echo '<h2>Switch és Match</h2>';
/** @todo Órai feladat: var examResult (0-5)
 * ki kell íratni:
 * 0-1: Sikertelen vizsga
 * 2-4: Sikeres vizsga
 * 5: Sikeres vizsga dícsérettel
 * minden egyéb: ismeretlen vizsgaeredmény
 */

echo '<br>';
function examResult($res): string {
    if ($res <= 1) {
        $msg = 'Sikertelen vizsga!';
    } elseif (2 <= $res && $res <= 4) {
        $msg = 'Sikeres vizsga!';
    } elseif ($res === 5) {
        $msg = 'Sikeres vizsga dícsérettel!';
    } else {
        $msg = 'ismeretlen vizsgaeredmény!';
    }
    return $msg;
}
$r = 3;
echo(examResult($r));

//switch -> gyengén típusos, nem hat rá a strict_types beállítás
echo '<br>'.'switch: ';
$examResult2 =rand(0, 10);
switch ($examResult2) {
    case 0:
    case 1:
        echo 'Sikertelen vizsga!';
        break;
    case '2':
    case '3':
    case '4':
        echo 'Sikeres vizsga!';
        break;
    case 5:
        echo 'Sikeres vizsga dícsérettel!';
        break;
    default:
        echo 'Ismeretlen vizsgaeredmény!';
        break;
}

//match -> erősen típusos, erre hat a strict_types beállítás és csak PHP8-ban működik, univerzálisabb a switchnél
echo '<br>'.'match: ';
match ($examResult2) {
    0 => print 'Sikertelen vizsga',
    1 => print 'Sikertelen vizsga',
    2 => print 'Sikeres vizsga!',
    3 => print 'Sikeres vizsga!',
    4 => print 'Sikeres vizsga!',
    5 => print 'Sikeres vizsga dícsérettel!',
    default => print 'Ismeretlen vizsgaeredmény!'
};

//match2
echo '<br>'.'match2: ';
$examResult3 = 1;
$examResultDisplay = match ($examResult3) {
    0 > 1 => print 'Sikertelen vizsga', // kifejezés is megadható
    2,3,4 => print 'Sikeres vizsga!',
    5 => print 'Sikeres vizsga dícsérettel!',
    default => print 'Ismeretlen vizsgaeredmény!'
};

echo '<br>';
echo $examResultDisplay; // van visszatérési érték: 1
