<?php
    $address1 = filter_input(INPUT_POST, "address1", FILTER_SANITIZE_SPECIAL_CHARS);
    $address2 = filter_input(INPUT_POST, "address2", FILTER_SANITIZE_SPECIAL_CHARS);
    $postcode = filter_input(INPUT_POST, "postcode", FILTER_SANITIZE_SPECIAL_CHARS);
    $city = filter_input(INPUT_POST, "city", FILTER_SANITIZE_SPECIAL_CHARS);
    $country = filter_input(INPUT_POST, "country", FILTER_SANITIZE_SPECIAL_CHARS);
?>
<!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Cím megadása</title>
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css" type="text/css">
    <style>
        #container {
            width: 40%;
            margin: 10px auto 0 auto;
            text-align: center;
            border: 1px solid darkslategrey;
            border-radius: 10px;
        }
        label {
            font-weight: bold;
        }
        .form-group {
            margin-top: 10px;
            margin-bottom: 10px;
        }
    </style>
</head>
<body>
<div id="container">
    <form method="post">
        <div class="form-group">
            <div class="form-control">
                <label for="address1">Címsor 1</label>
                <input id="address1" name="address1" type="text" placeholder="Adja meg a címét!" required autofocus>
            </div>
            <div class="form-control">
                <label for="address2">Címsor 2</label>
                <input id="address2" name="address2" type="text" placeholder="(lépcsőház, emelet, ajtó) - opcionális">
            </div>
            <div class="form-control">
                <label for="postcode">Irányítószám</label>
                <input id="postcode" name="postcode" pattern="[0-9]{4}" placeholder="1234" required>
            </div>
            <div class="form-control">
                <label for="city">Város</label>
                <input id="city" name="city" type="text" placeholder="város" required>
            </div>
            <div class="form-control">
                <label for="country">Ország</label>
                <input id="country" name="country" type="text" placeholder="ország" required>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Elküld</button>
        </div>
    </form>
</div>

<?php
/*    echo $address1.'<br>';
    echo $address2.'<br>';
    echo $postcode.'<br>';
    echo $city.'<br>';
    echo $country.'<br>';
*/?>

<!--@todo
https://docs.emmet.io/cheat-sheet/
Cím megadása, űrlap elkészítése és a kapott adatok hibakezelése
Címsor 1 - kötelező
Címsor 2 - nem kötelező
Irányítószám: kötelező 4 szám
Város - kötelező
Ország - kötelező

-cdnről bootstrapet vagy tailwindet és legyenek classok
-megvalósítás legyen egy fájlos
-->
<script src="../bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
