<?php
declare(strict_types = 1);
// osztály betöltése
require_once '../../Product.php';

echo '<pre>';
// objektum létrehozása
$product = new Product('Termék 1', 1600);
var_dump($product);
//die();

//$product -> name = 'Termék 1 új';
//$product -> price = 2000.00;
//var_dump($product -> name);

echo 'Termék ára: '.$product->getPrice();
//exit;
echo '<br>';
echo 'Termék neve: '.$product->getName();
echo '<br>';

$product2 = new Product('Termék 2', 1990.00);
var_dump($product2);

// adjunk áfát a termékhez
$product2->addTax(27);
var_dump($product2);

$product3 = new Product('Termék 3', 10000);
var_dump($product3);

// adjunk kedvezményt a termékre
$product3->addDiscount(0);
var_dump($product3);

echo $product->display();
echo $product2->display();
echo $product3->display();

// echo $product -> ezt nem tudja alapvetően stringgé konvertálni alapból --> __toString magic method
echo $product;
unset($product);
$product2 = null;

// 27% ÁFA, 10% kedvezmény
var_dump($product3);
$product3->addTax(27)->addDiscount(10)->display();

// egy lépésben
$className = 'Product'; // nem javasolt
$product4 = (new Product('Termék 4', 25000)) // tördelés: olvashatóság!!
    ->addTax(27)
    ?->addDiscount(10); // nullsafe operátor
echo $product4;
