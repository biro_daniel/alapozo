<?php
/**
 * @todo HF
 * Készíts egy Address osztályt a címek kezelésére (helyezd névtérbe!),
 * 3 címtípus lehet: állandó, szállítási és fizetési
 * a propertyk igazodjanak a már elkészített űrlap elemeihez
 *
 * egy test_address fájlban hozz létre ciklusban X címobjektumot,
 * az adatokat a faker segítségével generáld le a létrehozásukhoz
 * és egyenként dumpold az elemeket!
*/
require __DIR__."/../../vendor/autoload.php";

use App\Enums\Status;
use App\PaymentGateway\PayPal\Transaction;

$transaction = new Transaction(5000, 'T1'); // példányosítás

$transaction->setStatus(Status::PAID);

var_dump($transaction::getCount());
var_dump($transaction::$amount);
var_dump($transaction::process()); // nem statikus eljárás meghívása: hiba

/*
$transaction = new Transaction(5000, 'T1');
$transaction = new Transaction(5000, 'T1');
$transaction = new Transaction(5000, 'T1');
$transaction = new Transaction(5000, 'T1');
$transaction = new Transaction(5000, 'T1');
$transaction = new Transaction(5000, 'T1');
*/

//echo $transaction::STATUS_PAID; // így, objektumból is elérhető

//echo $transaction::class;
