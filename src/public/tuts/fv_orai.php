<?php
/**
 * @todo: Órai levezető feladat:
 * függvény létrehozása:
 * bemenet: egy vagy több szöveg
 * a bemeneti stringből készítsen egy ékezetek nélküli változatot, amiben csak kisbetűk, számok, '_' és '-' szerepel, minden más karaktert cserélje '-'-re, a visszatérés elején és végén nem szerepelhet sem '_', sem '-'
 * visszatérés: tömb vagy szöveg
 * tesztelés: '2021-ÁrvÍzTŰrő% tükÖRFúrógÉP---_[]..txt??' => '2021-arvizturo-tukorfurogep-_-.txt'
 * @todo: HF: fájlvégződés maradjon (ha fájlnév lenne)
*/

function textBeautifier($strings): array|string {
    if (preg_match('/[.][a-z]{3}/', substr($strings, -4)) > 0) {
        $extension = substr($strings, -4);
        $strings = substr($strings, 0, strlen($strings) - 4);
    } else {
        $extension = '';
    }

    $orig_characters = ['á', 'é', 'í', 'ó', 'ö', 'ő', 'ú', 'ü', 'ű', 'Á', 'É', 'Í', 'Ó', 'Ö', 'Ő', 'Ú', 'Ü', 'Ű'];
    $transformed_characters = ['a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u', 'a', 'e', 'i', 'o', 'o', 'o', 'u', 'u', 'u'];
    $clean_string = str_replace($orig_characters, $transformed_characters, $strings);
    $clean_string = strtolower(preg_replace('/[^a-zA-Z0-9-_]/','-', $clean_string));
    $clean_string = preg_replace('/[-]{2,}|[_]{2,}/','-', $clean_string);
    return trim($clean_string, '-_').$extension;
}

echo textBeautifier('2021-ÁrvÍzTŰrő% tükÖRFúrógÉP---_[]..txt??');
echo '<br>';
echo textBeautifier('2021-ÁrvÍzTŰrő% tükÖRFúrógÉP---_[]..txt');
echo '<br>';
echo textBeautifier('ékezetes_fájlnév_2021-ből.doc');
echo '<br>';
echo textBeautifier('asdf_+!%/.-0121324---áéúőougjrewrü45**');
echo '<br>';

// négybetűs fájlkiterjesztések? ez teljesen sztenderdizált?