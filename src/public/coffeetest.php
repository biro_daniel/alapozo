<?php

use const App\CoffeeMachine;

require_once __DIR__ . "/../vendor/autoload.php";

/*$coffeeMachine = new \App\CoffeeMachine\CoffeeMachine();
$coffeeMachine->makeEspresso();

$latteMachine = new \App\CoffeeMachine\LatteMachine();
$latteMachine->makeEspresso();
$latteMachine->setTypeOfMilk('Gőzölt tej');
$latteMachine->makeLatte();

$cappuccinoMachine = new \App\CoffeeMachine\CappuccinoMachine();
$cappuccinoMachine->makeEspresso();
$cappuccinoMachine->makeCappuccino();

$allInOneCoffeeMachine = new \App\CoffeeMachine\AllInOneCoffeeMachine();
$allInOneCoffeeMachine->makeEspresso();
$allInOneCoffeeMachine->makeLatte();
$allInOneCoffeeMachine->makeCappuccino();*/

//\App\CoffeeMachine\LatteMachine::test();
\App\CoffeeMachine\LatteMachine::$a = 'TEST A';
\App\CoffeeMachine\AllInOneCoffeeMachine::$a = 'TEST B';

echo \App\CoffeeMachine\LatteMachine::$a . ' | '. \App\CoffeeMachine\AllInOneCoffeeMachine::$a . PHP_EOL;

/** @todo HF:
 * az öröklésben a statikus elemek az objektumok szempontjából globálisak -
 * teszteld le, hogy TRAIT esetén hogyan működik
 * hint: használható a traitben a __CLASS__
 */
