<?php

require_once __DIR__ . "/../vendor/autoload.php";

use App\Invoice;
use App\Customer;

//set_exception_handler(function(Throwable $e){
//    var_dump($e->getMessage());
//});

//echo array_rand([], 1);

# Teszt érvénytelen összeggel
$invoice = new Invoice(new Customer(['name'=>'test']));
try{
    $invoice->process(-1000);
}catch(\App\Exception\InvoiceException $e){
    echo $e->getMessage() . PHP_EOL;
}

# Teszt számlázási adatok nélkül de jó összeggel
$invoice = new Invoice(new Customer());
try{
    $invoice->process(1000);
}catch(\App\Exception\InvoiceException $e){
    echo $e->getMessage() . PHP_EOL;
}

# Teszt jó adatokkal
$invoice = new Invoice(new Customer(['name'=>'test']));
try{
    $invoice->process(1000);
}catch(\App\Exception\InvoiceException $e){
    echo $e->getMessage() . PHP_EOL;
}


/*var_dump(process());

function process()
{
    $invoice = new Invoice(new Customer(['name' => 'testuser']));
    try {

        $invoice->process(-1000);
        return true;

    } catch (\Exception $e) {
        echo $e->getMessage() . ' ' . $e->getFile() . ':' . $e->getLine() . PHP_EOL;
        echo 'Hiba csúszott a gépezetbe!' . PHP_EOL;

        return false;
    } finally { //ez mindenképp le fog futni
        echo 'Utolsó blokk.' . PHP_EOL;

        //return -1; //mindenképp ezt hajtja végre ha itt van return

    }
}*/


echo '**Vége**' . PHP_EOL;