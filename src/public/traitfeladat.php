<?php

require_once __DIR__ . "/../vendor/autoload.php";

/** @todo órai minifeladatok:
 * Metódus végrehajtási sorrend tesztelése: baseclass, childclass, trait: mi az erősorrend közöttük?
 * Statikus elem a traitből: lehet-e statikus elemet használni/felülírni?
 * Absztrakt metódus érkezhet-e traitből?
*/

// 1.
// baseclass: CoffeeMachine, childclass: LatteMachine, trait: LatteTrait
$latteMachine = new \App\CoffeeMachine\LatteMachine();
$latteMachine->orderTest();
echo '<br>';
$latteMachine->orderTest2();
echo '<br>';
/*
erősorrend metódusok esetén:
1. child class (saját metódus)
2. trait
3. base class
*/

// 2.
$latte = new App\CoffeeMachine\LatteMachine();
var_dump($latte);
var_dump(\App\CoffeeMachine\LatteMachine::$test);
// igen, lehet

// 3.
$latteMachine2 = new \App\CoffeeMachine\LatteMachine();
$latteMachine2->getMilk();
// lehet igen, csak létre kell hozni a metódust az osztályban is
//(szerződés osztállyal: contract: erre az interface való)

