<?php
require_once __DIR__ . '/../vendor/autoload.php';
#egysoros komment
//egysoros komment
/*
 * blokk komment
 */
/**
 * Docblock - lehet benne tageket használni
 *
 * @tag
 * @
 */

$transaction = new \App\Transaction();
$test_a = $transaction->a . PHP_EOL; #warning mert nem létezik '...még'
$transaction->a = 50; #docblock jelzés
echo $transaction->b;
$transaction->b = 32.4335;
echo $transaction->c;
$test_b = $transaction->b; #docblock jelzés
$transaction->c = 123;
$test_c = $transaction->c;
$transaction->d = 'test'; #nem létező propertyt próbálok elérni (nincs a docblockban sem)

/*
echo $transaction->a . PHP_EOL;
echo $transaction->unexisting . PHP_EOL;
var_dump($transaction);
*/

/**
 * @todo [KÉSZ!] Transaction osztály kiegészítése, hogy megfelelő típusú a, b és c elemekkel lehessen csak feltölteni,
 * a többit hagyja figyelmen kívül vagy tájékoztasson a nem létező elem írásról- és olvasásról
 * a csak -read $a elem a property első(!) kiolvasásakor keletkezzen az objektumban (bármilyen generált string jó)
 * konstruktor nélkül
 */

/**
 * @todo 2: típusok ellenőrzése (ha nem megfelelőtípusú adatot próbálunk megadni, ne vegye fel, hanem dobjon hibát)
 * A folyamatot egészítsd ki trigger_error notice típusú üzenetekkel!
 */
