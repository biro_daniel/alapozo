<?php
require __DIR__ . "/../vendor/autoload.php";

use App\Toaster;
use App\ToasterPro;

$toaster = new Toaster();
$toaster->addSlice('Kenyér');
$toaster->addSlice('Kenyér');
$toaster->addSlice('Kenyér'); // csak kettőt fog megsütni mert az a max.
$toaster->toast();

$protoast = new ToasterPro();
//$protoast->size = 10 // ha publikus a $size, akkor itt megtörhető a zártság elve!!

$protoast->addSlice('Kenyér');
$protoast->addSlice('Kenyér');
$protoast->addSlice('Kenyér');
$protoast->addSlice('Kenyér');
$protoast->addSlice('Kenyér');
$protoast->toastBagel();

//php public/toast_test.php