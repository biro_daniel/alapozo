<?php
declare(strict_types = 1);
require_once __DIR__ . "/../vendor/autoload.php";

use App\CustomInvoice;
use App\Invoice;

$invoice1 = new Invoice(new App\Customer('Vásárló 1'), 100, 'Számla');
$invoice2 = new CustomInvoice(new App\Customer('Vásárló 1'), 100, 'Számla');

/* elakad, mert végtelen csatolást okoz
$invoice1->attachedInvoice = $invoice2;
$invoice2->attachedInvoice = $invoice1;
*/

//$invoice3 = $invoice1;

echo '$invoice1 == $invoice2'.PHP_EOL;
var_dump($invoice1 == $invoice2);

echo '$invoice1 === $invoice2'.PHP_EOL;
var_dump($invoice1 === $invoice2); // identity operator

var_dump($invoice1, $invoice2);

/*
echo '$invoice1 == $invoice3'.PHP_EOL;
var_dump($invoice1 == $invoice3);

echo '$invoice1 === $invoice3'.PHP_EOL;
var_dump($invoice1 === $invoice3);
$invoice1->amount = 5000;
$invoice3->description = 'Számla 3';
var_dump($invoice1, $invoice3);
*/
