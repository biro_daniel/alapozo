<?php

use App\Invoice;

require __DIR__ . "/../vendor/autoload.php"; // osztályok betöltődése automatikusan

$invoice = new App\Invoice();
echo '<pre>';

//$invoice->invoice::$amount; // ilyen statikus elemekre nem működnek az alap magic methodok
/* class eredeti tartalmával
$invoice->amount = 5000;
var_dump($invoice, isset($invoice->amount));
unset($invoice->amount);
var_dump(isset($invoice->amount));
*/

//$invoice::process(100, 400, 900); // ez a statikus
//$invoice->process(5000, 'Ez egy teszt...');

//var_dump($invoice instanceof Stringable);
// Stringable: egy interface, ami megköveteli, hogy string legyen a visszaadott érték

// hívható-e egy adott elem?
//var_dump(is_callable($invoice));
//$invoice();

/* toStringhez:
$a = 'a: '.$invoice;
var_dump($a);
*/

var_dump($invoice);
var_dump($invoice,$invoice->accountNumber);

// vagy:
$class = new Invoice();
$reflectionProperty = new ReflectionProperty(Invoice::class, 'accountNumber');
$reflectionProperty->setAccessible(true);
var_dump($reflectionProperty->getValue($class));
