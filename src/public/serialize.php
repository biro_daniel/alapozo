<?php
require_once __DIR__ . '/../vendor/autoload.php';
use App\Invoice;

$invoice = new Invoice(100, 'Számla 1', '1234-5678-0000-0000');

//echo serialize(true) . PHP_EOL,
//    serialize(1) . PHP_EOL,
//    serialize(3.14) . PHP_EOL,
//    serialize('valami') . PHP_EOL,
//    serialize([1, 2, 3]) . PHP_EOL,
//    serialize([
//        'a' => 1,
//        'b' => 2,
//    ]) . PHP_EOL;

//var_dump(unserialize(serialize(['a' => 1, 'b' => 2])));
//echo serialize($invoice) . PHP_EOL;

$mySerializedObject = serialize($invoice);
echo $mySerializedObject . PHP_EOL;
// pl: 'O:11:"App\Invoice":1:{s:2:"id";s:20:"SZAMLA_61b0fde8cf6c9";}'

$invoice2 = unserialize($mySerializedObject); // ez az objektum idemásolva

var_dump($invoice2);
