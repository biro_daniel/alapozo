<?php

require __DIR__ . "/../vendor/autoload.php";

use App\Field\{Text, Checkbox, Radio, Textarea, Email};

$inputs = [
    //new Field('baseField'),
    new Text('textField', ['one', 'two', 'three']),
    //new Boolean('boolField'),
    new Radio('radioField', 'one'),
    new Checkbox('checkboxField', 'two'),
    new Textarea('textAreaField', 3),
    new Textarea('textAreaField', true),
    new Email('emailField', ['randomClass1', 'randomClass2']),
    new Email('emailField')
];

foreach ($inputs as $field){
    echo $field->render().'<br>';
}
