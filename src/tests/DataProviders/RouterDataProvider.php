<?php

namespace Tests\DataProviders;

class RouterDataProvider
{
    public function routeNotFoundCases(): array
    {
        return [
            ['/users', 'put'], // correct route, incorrect method (not found)
            ['invoices', 'post'], // correct method, incorrect route
            ['/users', 'get'], // class not found
            ['/users', 'post'] // method does not exist ('store' was provided)
        ]; // de mindegyik ugyanazt az excpetiont fogja dobni
    }
}