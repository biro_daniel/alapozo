<?php
$fileCount = filter_input(INPUT_POST, 'fileCount');
$ret = '<h1>Fájlok feltöltése</h1>';
$ret .= '<form enctype="multipart/form-data" method="post" action="/upload">
         <span>PDF formátumú dokumentumok (max. méret: 1 MB/fájl)</span>
         <br>';
for ($i = 1; $i <= $fileCount; $i++) {
    $ret .= '<label>
             <input type="file" name="filesToUpload[]">
             </label>
             <br>';
}

$ret .= '<button type="submit">Feltöltés</button>
                 </form>';
echo $ret;
