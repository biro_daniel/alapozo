<!doctype html>
<html lang="en" style="height: 100%;">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body style="height:100%;
             display: flex;
             justify-content:center;
             align-items:center;
             text-align:center;
             background-color: lightblue">
<div>
<h1>A keresett URL nem elérhető.</h1>
<h2>Ezt próbáltad elérni:
    <br>
    <p style="color: darkred; margin-top: 10px">
    <?php echo $_SERVER['REQUEST_URI'] ?>
    </p>
</h2>
<h4>A pisztácia elfogyott, csokoládé nem is volt... :(</h4>
</div>
</body>
</html>

